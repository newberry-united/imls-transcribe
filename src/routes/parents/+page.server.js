
// import itemsWithParents from '$data/itemsWithParents.json';

import cortexmatches from '$data/cortex-matches.json';


let list = cortexmatches.map(i => i[0]).filter((i, idx) => idx % 30 === 0)
console.log(list.length)
export async function load() {
    let retVal = []
    await Promise.all(list.map(async (d, idx) => {
        console.log(d, idx)
        console.log(`https://collections.newberry.org/API/search/v3.0/search?query=Text:${d}%20AND%20Path:Library/Uploads/mms/20210916-mms_IMLS%20Cares/*&fields=CoreField.Identifier,Document.CortexParentLink,Document.CortexShareLink,Document.ParentIdentifier,Title&format=json`)
        const apicall = await fetch(`https://collections.newberry.org/API/search/v3.0/search?query=Text:${d}%20AND%20Path:Library/Uploads/mms/20210916-mms_IMLS%20Cares/*&fields=CoreField.Identifier,Document.CortexParentLink,Document.CortexShareLink,Document.ParentIdentifier,Title&format=json`);
        const apidata = await apicall.json();
        retVal.push(apidata)

    }));
	return retVal;
}
