import json, re, csv
from pathlib import Path

with open('./bibidTranscriptions.json', 'r') as f:
    t=f.read()
y = json.loads(t)

with open('./cortexData.json', 'r') as c:
    u=c.read()
z = json.loads(u)

outputArr = []
csvArr = [["uniqId", "transcription"]]

for item in y:
    # print(y)
    for i in item["pages"]:
        uOmekafn = Path(i["pageorigfilename"])
        uOmekafn = uOmekafn.with_suffix('')
        # print(uOmekafn)
        uOmekafn = str(uOmekafn).replace("_o2","")
        uOmekafn = uOmekafn.replace(' copy','')
        for citem in z:
            if uOmekafn in citem["origfn"]:
                success = [i, citem] 
                outputArr.append(success)
                transc=i["transcription"].replace('\n'," ")
                csvData = [citem["unid"],transc]
                csvArr.append(csvData)

filename='succFile.json'
with open(filename, 'w', encoding='utf-8') as f:
    json.dump(outputArr, f, ensure_ascii=False, indent=4)
print('succ array is ' + str(len(outputArr)) + ' items long')

with open('succCsv.csv', 'w', encoding='WINDOWS-1252') as f:
    writer = csv.writer(f)
    writer.writerows(csvArr)
