import json, re, csv
from pathlib import Path

urlStart = 'https://collections.newberry.org/API/search/v3.0/search?query='
urlSearchField = 'CoreField.OriginalFileName:'
urlEnd = '&fields=SystemIdentifier,CoreField.Unique-identifier,Title,new.BibID-Link,CoreField.OriginalFileName&format=json&countperpage=20000'

with open('../../../data/itemTranscriptions.json', 'r') as f:
    t=f.read()
y = json.loads(t)

outputArray = []


bibidlist = [
    "1315",
    "1316",
    "1320",
    "1326",
    "1328",
    "855",
    "858",
    "897",
    "919",
    "1314",
    "1325",
    "1329",
    "834",
    "850",
    "860",
    "871",
    "880",
    "883",
    "889",
    "964",
    "968",
    "976",
    "1029",
    "1036",
    "1044",
    "1229",
    "1313",
    "1327",
    "829",
    "845",
    "848",
    "865",
    "884",
    "894",
    "965",
    "973",
    "1024",
    "1041",
    "804",
    "840",
    "842",
    "846",
    "847",
    "849",
    "853",
    "861",
    "866",
    "907",
    "917",
    "930",
    "933",
    "961",
    "962",
    "963",
    "1045",
    "1065",
    "1075",
    "1361",
    "859",
    "1067",
    "1288",
    "1308",
    "910",
    "938",
    "1070",
    "1071",
    "1307",
    "836",
    "873",
    "936",
    "1040",
    "928",
    "966",
    "1042",
    "1047",
    "1069",
    "1076",
    "1191",
    "891",
    "975",
    "1276",
    "923",
    "967",
    "939",
    "1068",
    "1077",
    "1290",
    "879",
    "1137",
    "1356",
    "996",
    "896",
    "914",
    "1264",
    "1312",
    "872",
    "1066",
    "913",
    "1017",
    "1286",
    "832",
    "1037",
    "1230",
    "1324",
    "1360",
    "932",
    "1038",
    "1039",
    "1292",
    "1266",
    "1074",
    "864",
    "1287",
    "1349",
    "1012",
    "1014",
    "1372",
    "841",
    "1359",
    "1306",
    "979",
    "1368",
    "1228",
    "1086",
    "1399",
    "1369",
    "931",
    "972",
    "1212",
    "1355",
    "1366",
    "1265",
    "977",
    "1300",
    "1205",
    "1367",
    "1203",
    "902",
    "1303",
    "1073",
    "981",
    "1201",
    "1305",
    "1018",
    "1289",
    "1016",
    "1259",
    "1231",
    "1197",
    "935",
    "1371",
    "995",
    "1207",
    "1209",
    "1301",
    "1210",
    "1293",
    "1199",
    "1302",
    "980",
    "1083",
    "1261",
    "905",
    "1295",
    "38",
    "1362",
    "1363",
    "937",
    "1072",
    "1227",
    "1370",
    "1051",
    "921",
    "1046",
    "1204",
    "1050",
    "1060",
    "1026",
    "1064",
    "1297",
    "1048",
    "1054",
    "1058",
    "1062",
    "1298",
    "1057",
    "969",
    "970",
    "1084",
    "1085",
    "1063",
    "1365",
    "1053",
    "1049",
    "1055",
    "1015",
    "1056",
    "1061",
    "900",
    "1052",
    "1059",
    "971",
    "1294",
    "1206",
    "1299",
    "1202",
    "1211",
    "922",
    "943",
    "944",
    "1193",
    "1304",
    "1310",
    "1311",
    "1309",
    "994",
    "1285",
    "212",
    "211",
    "210",
    "213",
    "1194",
    "1198",
    "1195",
    "1353",
    "1167",
    "1196",
    "830",
    "1358",
    "1330",
    "1164"
]


for item in y:
    if item["id"] not in bibidlist:
    # print(y)
        for i in item["pages"]:
            uOmekafn = Path(i["pageorigfilename"])
            uOmekafn = uOmekafn.with_suffix('')
            # print(uOmekafn)
            uOmekafn = str(uOmekafn).replace("_o2","")
            uOmekafn = uOmekafn.replace(' copy','')
            outputArray.append(urlStart + urlSearchField + uOmekafn + urlEnd)

with open('ctxUrls.txt', 'w', encoding='WINDOWS-1252') as f:
    for line in outputArray:
        f.write("%s\n" % line)


print('we have ' + str(len(outputArray)) + ' urls')