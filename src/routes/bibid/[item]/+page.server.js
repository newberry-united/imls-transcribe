export const load = async ({ params, fetch }) => {
    let datapath = '/src/data/transc/item_' + params.item + '.json'
    let results = await fetch(datapath)
    let data = await results.json()
    let bibid = await data.cataloglink.match(/99([0-9]{2,15})8805867/)
        ? data.cataloglink.match(/99([0-9]{2,15})8805867/)[1]
        : data.cataloglink.match(/BBRecID=([0-9]{2,15})/) ? data.cataloglink.match(/BBRecID=([0-9]{2,15})/)[1] : null;
    let cdata = []
    if ( bibid){
        console.log(data.cataloglink)
        let cdatapath = 'https://collections.newberry.org/API/search/v3.0/search?query=new.BibID-Link:' + bibid + '&fields=CoreField.Unique-identifier,Title,new.BibID-Link,CoreField.OriginalFileName&format=json'
        console.log(cdatapath)
        let cresults = await fetch(cdatapath)
        cdata = await cresults.json()
    }
    return [data, cdata, bibid]
 }