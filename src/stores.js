import { readable, derived } from 'svelte/store';
import imlsTransc from '$data/transc.json';
import items from '$data/items.json';
import { page } from '$app/stores';

export const imlsIds = readable(
	[
		'1373',
		'1376',
		'1381',
		'1386',
		'1389',
		'1393',
		'1403',
		'1406',
		'1374',
		'1378',
		'1382',
		'1387',
		'1390',
		'1394',
		'1404',
		'1407',
		'1375',
		'1380',
		'1383',
		'1388',
		'1391',
		'1402',
		'1405'
	].sort()
);

export const textId = derived([page], ([$page]) => $page.params.route);
export const textContent = derived(
	[textId],
	([$textId]) => imlsTransc.filter((item) => item.id == $textId)[0]
);
export const itemData = derived(
	[textId],
	([$textId]) => items.filter((item) => item.id == $textId)[0]
);
