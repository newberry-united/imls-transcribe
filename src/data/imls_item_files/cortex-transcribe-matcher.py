import csv, os, json

directory = './'

outputArray = []
f = open('../../../cortex-matches.json')
cortexMatches = json.load(f)

for filename in os.listdir(directory):
    # f = os.path.join(directory, filename)
    if filename.endswith('.json'):
        print(filename)
        ftwo = open(os.path.join(directory, filename))
        jsonData = json.load(ftwo)
        for item in jsonData:
            outputObject = {
                "omekaitem": item["item"]["id"],
                "omekapage": item["id"]

            }
            for cm in cortexMatches:
                if cm[2] == str(item["id"]):
                    outputObject["cortexID"] = cm[0]
            for et in item["element_texts"]:
                if et["element"]["name"] == "Transcription":
                    transc = et["text"].replace('\n',' ').replace('  ',' ')
                    outputObject["transcription"] = transc
            if 'cortexID' in outputObject:
                outputArray.append(outputObject)


jsonOutput = json.dumps(outputArray, indent=4)

with open("cortex_transcribe_matches.json", "w") as outfile:
    outfile.write(jsonOutput)

with open('cortex_transcribe_matches.csv', mode='w') as outputFile:
    writer = csv.DictWriter(outputFile, fieldnames=outputArray[0].keys())
    writer.writeheader()
    for data in outputArray:
        writer.writerow(data)
