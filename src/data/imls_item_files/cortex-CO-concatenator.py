import csv, os, json

directory = './'

outputArray = []
f = open('../../../cortex-matches.json')
cortexMatches = json.load(f)

for filename in os.listdir(directory):
    # f = os.path.join(directory, filename)
    if filename.endswith('.json'):
        ftwo = open(os.path.join(directory, filename))
        jsonData = json.load(ftwo)
        for item in jsonData:
            outputObject = {}
            for cm in cortexMatches:
                if cm[2] == str(item["id"]):
                    outputObject["cortexID"] = cm[0]
            for et in item["element_texts"]:
                if et["element"]["name"] == "Transcription":
                    transc = et["text"].replace('\n',' ').replace('  ',' ')
                    outputObject["transcription"] = transc
            if 'cortexID' in outputObject:
                outputArray.append(outputObject)

with open('cortex_transcribe_matches.csv', mode='w') as outputFile:
    writer = csv.DictWriter(outputFile, fieldnames=outputArray[0].keys())
    writer.writeheader()
    for data in outputArray:
        writer.writerow(data)1378.json
1380.json
1381.json
1382.json
1383.json
1391.json
1393.json
1394.json
cortex-CO-concatenator.py
cortex-transcribe-matcher.py
cortex_transcribe_matches.csv
cortex_transcribe_matches.csv.zip
getter.sh
