{
    "id": "56",
    "cataloglink": "https://mms.newberry.org/xml/xml_files/Everett.xml#series2",
    "pages": [
        {
            "pageid": 17763,
            "pagefilename": "0a424c368a6faa2b72290dbbee297e3c.jpg",
            "pageorigfilename": "mms_everett_ser_02_box_15_fl_553_001_001_o2.jpg",
            "transcription": "Our Woods\nOur own dear woods, how I do love them, with their knotty old trunks, and green cushioned stones; their fragrant beds of spring beauties, and noles covered with white violets, or tall mosses, whose stems and heads, (as we children were accustomed to call them) it was our delight to form into fairy dog-chains, beautiful strings of coral beads, and numerous other fancy articles. It mattered not that they were all precisely alike in form; we gave them different names, and it was wonderful to see the changes so produced in their appearance. The red and white strawberries also, that abounded there, afforded us no small sources of pleasure. Many is the overflowing handful I have plucked of them, and many the sweet mouthful I have eaten. When I was a “wee thing,” a journey to the woods, and a ramble there, was a realization of my highest conceptions"
        },
        {
            "pageid": 17764,
            "pagefilename": "10a3231c0a61d3fa7c686a80d6f6c875.jpg",
            "pageorigfilename": "mms_everett_ser_02_box_15_fl_553_001_002_o2.jpg",
            "transcription": "of attainable felicity, and our woods, in my estimation, always stood preeminent among all living forests. The few tall poplars that, as we entered, we found towering above our heads, and springing up in diminutive trees all around us, sporting their thin, silvery leaves in the sun's light the great, generous beachnut tree the sweet-barked and saw-leaved birch - the deep, abiding greenness of the hemlock, and above all, the graceful queenly elm, with its uppermost branches gently bending, and its slender trunk so closely encircled by the loving verdure, were, and are still, objects of my most intense admiration. Often have I lingered for hours beneath their shade, busily searching for wild flowers or seated on some little hill, gazing up through the thick branches to the far heavens, so calmly and uninterruptedly blue, or bespangled with white transparent clouds, (of course I never looked up in dull weather) which I loved to regard as kind, watchful angel friends. After I had seen enough of the sky, Or I should rather say, after I had"
        },
        {
            "pageid": 17765,
            "pagefilename": "aa45198c7a007b214b8f6ec7f129328b.jpg",
            "pageorigfilename": "mms_everett_ser_02_box_15_fl_553_001_003_o2.jpg",
            "transcription": "ceased gazing, for how could I see enough? I generally staid awhile to find wreaths of one-berry leaves, with which to crown some merry playmate.\nThe pleasure of many of these walks was greatly enhanced by being shared with precious friends, some of whom have now found other homes, far away from ours, and some now live ‘mid fairer fields which beyond Jordan's gloomy stream, \"stand dressed in living green\" ___ \nI remember how I used to run merrily down the pasture, with a great basket where I could store away plenty of pretty flowers, accompanied by brother with his jackknife, which quickly severed some nice branches, with which we adorned the sick room and cheered the sad heart of our darling drooping May-flower at home."
        },
        {
            "pageid": 17766,
            "pagefilename": "6f68286cf807b71398b761e939dd26b3.jpg",
            "pageorigfilename": "mms_everett_ser_02_box_15_fl_553_001_004_o2.jpg",
            "transcription": "Cynthia H. Everett \nNo. 2"
        },
        {
            "pageid": 17767,
            "pagefilename": "aefc2d9b8b4513fe86b896e36caf6b46.jpg",
            "pageorigfilename": "mms_everett_ser_02_box_15_fl_553_002_001_o2.jpg",
            "transcription": "Saturday Night\nWhat an eventful season is this!  How our thoughts linger between earth and heaven!  We have been mingling with the world and its memories still crowd upon us.  With varying feelings we recall the events of the past week.  We remember its vexations and its joys.  We live again those moments of conflict with temptation, muse on those sweet communings with heaven.  We think of our intercourse with our friends.  Perchance the memory of their more than wanted kindness makes the heart beat"
        },
        {
            "pageid": 17768,
            "pagefilename": "acb14c18a7c43f2a82d9b2f5d32cdb72.jpg",
            "pageorigfilename": "mms_everett_ser_02_box_15_fl_553_002_002_o2.jpg",
            "transcription": "with pleasure; again the recollection of some unadvised word or action brings the quick flush of mortification to the cheek.  Then we strive to bring our thoughts to better, holier things; knowing that these busy scenes are passed, and the morrow will usher in the holy Sabbath morn.   We feel that our hearts are tarnished with earthly things, and need to be made pure; our thoughts have been clinging to the dust, and need to be turned heavenward; for during the approaching sacred hours we are taught that we should \"honour the Lord, not doing our own ways, nor finding our own pleasure, nor speaking our own words.\" \n\n O Happy are we if Saturday night- yes, and the peaceful Sabbath morn finds us not so wholly engrossed in our own pleasure, that this commandment"
        },
        {
            "pageid": 17769,
            "pagefilename": "9dbfee23a882476d69b7c6fcd698b83a.jpg",
            "pageorigfilename": "mms_everett_ser_02_box_15_fl_553_002_003_o2.jpg",
            "transcription": "will seem uncongenial to us.  \nNew Hampton, Sept 4 1858"
        },
        {
            "pageid": 17770,
            "pagefilename": "1ec49e5149f65bd1b913bf2ddf78323e.jpg",
            "pageorigfilename": "mms_everett_ser_02_box_15_fl_553_002_004_o2.jpg",
            "transcription": "C.H. Everett\nWorld \nX"
        },
        {
            "pageid": 17771,
            "pagefilename": "35a809f3d0d2c108323177dfdb64cca0.jpg",
            "pageorigfilename": "mms_everett_ser_02_box_15_fl_553_003_001_o2.jpg",
            "transcription": "My Journey Home from School\nWe had advanced but a little on our homeward way, when I was offered a ride upon a load of wood, which offer was, of course, immediately accepted.  The driver very kindly assisted me to mount.  The ends of the logs formed the steps by which I ascended, and though they were not so graceful or easy of ascent as might be, they answered my purpose very well.  I was soon as comfortably seated on the uncouth looking logs, with a horse blanket cushion, as if I were on a velvet cushioned seat in a costly carriage.  I sat with my side towards the horses heads, and my face turned to the village before us.  Everything seemed anxious to render happiness.  Perhaps this appeared so to me, because I was just in the mood to be pleased.  But aside from my feelings the sight was truly pleasing.  Only the tops of the village houses could be seen from whose chimneys the smoke went up, curling gently through the air.  The snow fell so softly, that instead of dazzling or bewildering the sight it lent a beauty to the air through which we looked, making what was always admired appear still more lovely and over that, which like the rough rail fences, was really homely, it threw a mantle so pure and graceful, that we could not but call it beautiful.  Lightly and merrily it danced around the hills and forest beyond the village dusting them over with white.  My favorite hill was in its prime. Its form, the same as ever, arose like a pyramid, higher than any other eminence around, and its crowning tree still stood erect upon the top.  But it seemed to have a more than usual beauty"
        },
        {
            "pageid": 17772,
            "pagefilename": "24090eadad9e6cf9542a5c2cd98c9327.jpg",
            "pageorigfilename": "mms_everett_ser_02_box_15_fl_553_003_002_o2.jpg",
            "transcription": "A beauty borrowed from the heavens above. - And all this time I was riding on a load of wood.  Yes, as I sat carelessly there, admiring the scenery, the rougish little snow flakes,  as they dashed past me to the earth, were laughing at my odd appearance. \nRemsen December 21 1855\nCynthia H. Everett\nComposition No."
        },
        {
            "pageid": 17773,
            "pagefilename": "a24b18e89654d56d8eaf99d83a99615b.jpg",
            "pageorigfilename": "mms_everett_ser_02_box_15_fl_553_004_001_o2.jpg",
            "transcription": "A walk to the mountain\nIt was a pleasant summer afternoon when Mary Henry and Anna to see a mountain  When they reached the place they saw an ocean lying at the foot of the mountain. Henry picked up a wild flower which had but one leaf there said He is a flowr which we can have to put in our garden. Just then their cousins Elisabeth and her little sister Sarah came to then. How do you do said Mary what did you say about Ann? said Sarah"
        },
        {
            "pageid": 17774,
            "pagefilename": "c085f64e48f102a207681573cb75a7e1.jpg",
            "pageorigfilename": "mms_everett_ser_02_box_15_fl_553_004_002_o2.jpg",
            "transcription": "I don't see any Any? here. She did not say Any? but she said Annas  They could see one star before they got home.\n\n     Cynthia"
        },
        {
            "pageid": 17775,
            "pagefilename": "acc38438577d99e9e28eee4e91ce4366.jpg",
            "pageorigfilename": "mms_everett_ser_02_box_15_fl_553_005_001_o2.jpg",
            "transcription": "\"Tis Sunday-day, We must not play\"\nThus, a little friend of mine, only three years old sings to herself as she wanders around the room on Sabbath days. Often stopping in her ramble, will she climb upon the lounge; then, quickly hopping down again, will run perhaps to her little bureau, and take out dolly, or some other friendly toy, and, as she explains it, only \"make believe to play.\" If she spy kitty about to walk soberly from the room, with a spring she has her in her arms, and with a face of pleased contentment will seat herself upon the carpet, and stroke the prettily marked head, and pinch the soft white paws, though knowing well that hidden there, are those dangerous tools with which cats sometimes do so much mischief. She fears no harm, however, for \"Kitty loves Hattie, and will"
        },
        {
            "pageid": 17776,
            "pagefilename": "daaea26dd99b3731a0c6894624f54dc2.jpg",
            "pageorigfilename": "mms_everett_ser_02_box_15_fl_553_005_002_o2.jpg",
            "transcription": "not scratch her, no, no.\" Sometimes she tries to teach her pet, the Sunday words she loves so well, but her pupil is very inattentive, and will keep up a constant purring, despite the earnest efforts of her teacher. So Hattie soon wearies of her task, and leaving pussy on the floor tries to arrest the attention of her sister, who sits conning her Sabbath school book. She will place herself before her, and with her saucy head bent now on one side, then on the other, slowly repeat with a \"Tis Sunday-day, We must not play.\"\nDear little one, she knows God's holy day is not like other days, yet of the manner in which its sacred hours should be observed, she has but little idea. Still she is happy, happy all the day, whether amusing herself in her charming, baby way, or nestled close in her mother's arms; as she often lies in the hushed hour of twilight, listening to the low tones which speak of the wondrous love of Him who is \"Lord of the Sabbath\" of the time"
        },
        {
            "pageid": 17777,
            "pagefilename": "33458d373b2fedcb367e2e92eb2cce66.jpg",
            "pageorigfilename": "mms_everett_ser_02_box_15_fl_553_005_003_o2.jpg",
            "transcription": "when Jesus took little children, just like Hattie, in his arms, and blessed them of his many, many loving words and gentle deeds, then of his fear but death. How eager becomes the gaze of the listener, as her mother pictures the scene of our Saviour's ascension! as she tells how he assembled a company of his loving friends, and led them to the top of a tall mountain. the Mount of Olivet, and there told them to be ever mindful of one another, to remember and often speak of him, and to send tidings of his love all over the earth, and as he raised his hands to bless them, how the clouds parted above, and he was lifted up, up,up, higher, higher, higher, and still they gazed until the rosy clouds folded again, and angel bands, whence came floating down sweet strains of music, bore him to his Father's bosom. Then follow mysterious questions, such as children alone can ask, and only mothers can answer.\n\nAh! who may tell how much of heaven that little heart learns in these twilight hours? Who may say that in the quiet"
        },
        {
            "pageid": 17778,
            "pagefilename": "3d94dc50df9643a7eb56188fc13f7841.jpg",
            "pageorigfilename": "mms_everett_ser_02_box_15_fl_553_005_004_o2.jpg",
            "transcription": "which followed the mothers teachings, angels come not down and impart sweet truths which the poluted mind may never know? Can the impressions, so vivid now, ever leave her? Ah! no, nor will those oft repeated lines ever be forgotten.When the bright days of childhood shall have passed away,  the Sabbath will not find her pining she cannot join in the light laugh and trifling words of thoughtless friends, for in her heart will sound the echo of the words her mother taught her, \"'Tis is Sunday-day, we must not play.\"\nC.H. Everett\nWhitestown, Jan, 18. 1861."
        }
    ]
}