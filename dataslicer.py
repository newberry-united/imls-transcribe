import json

imlslist = [
    "1373",
    "1376",
    "1381",
    "1386",
    "1389",
    "1393",
    "1403",
    "1406",
    "1374",
    "1378",
    "1382",
    "1387",
    "1390",
    "1394",
    "1404",
    "1407",
    "1375",
    "1380",
    "1383",
    "1388",
    "1391",
    "1402",
    "1405"
]

# with open('./itemTranscriptions.json', 'r') as f:
#     t=f.read()
# y = json.loads(t)
with open('./items.json', 'r') as f:
    t=f.read()
y = json.loads(t)

outputdata = []
for item in y["items"]:
    if item["id"] in imlslist:
        outputdata.append(item)


with open('data.json', 'w', encoding='utf-8') as f:
    json.dump(outputdata, f, ensure_ascii=False, indent=4)